﻿using System;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using System.Text;
using System.Net;

namespace UNNISA
{
    class Program
    {
        [Obsolete]
        static void Main(string[] args)
        {
            try
            {
                DateTime dateLess = DateTime.Today.AddDays(-1);
                DateTime dataInitProcess = DateTime.Now;
                string Date = dateLess.ToString("dd-MM-yyyy");
                string startPath = @ConfigurationManager.AppSettings["path_start"];
                string final = @ConfigurationManager.AppSettings["path_final"];
                string[] fileEntries = Directory.GetFiles(startPath);
                string zipPath = @final+"teste_" + Date + " - UNNISA.zip";
                string logSucess = @ConfigurationManager.AppSettings["log_success"];
                StringBuilder err = new StringBuilder();
                

                 if (fileEntries.Length > 0)
                 {
                     if (!File.Exists(zipPath))
                     {
                         try
                         {
                            Console.WriteLine("Processo em andamento");
                            ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Fastest, false);
                        }
                        catch (Exception e)
                        {
                            
                            Console.WriteLine("{0} Exception caught.", e);
                            
                        }
                        //Delete files from root path
                        foreach (var file_name in fileEntries)
                        {
                            string fileName = file_name.Substring(startPath.Length + 1);
                            Console.WriteLine(fileName);
                            File.Delete(startPath + "/" + fileName);
                        }

                        Console.WriteLine("Processo concluído");
                        StringBuilder sb = new StringBuilder();
                        DateTime finish = DateTime.Now;
                        string dateFinish = finish.ToString("dd-MM-yyyy_HH:mm:ss");
                        string hostName = Dns.GetHostName();
                        string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                        sb.AppendLine("PROCESSO CONCLUÍDO");
                        sb.AppendLine("IP/Host:"+myIP);
                        sb.AppendLine("INICIO DO PROCESSAMENTO: "+ dataInitProcess);
                        sb.AppendLine("FINAL DO PROCESSAMENTO: "+  finish);
                        sb.AppendLine("STATUS FINAL: Info");
                        sb.AppendLine("QUANTIDADE DE ARQUIVOS COPIADOS: "+ fileEntries.Length);
                        sb.AppendLine("BATCH_CODE: "+ 0);
                        File.AppendAllText(logSucess+"info_"+dateFinish+"_log.txt", sb.ToString());
                        sb.Clear();                        
                     }else{
                        DateTime errorDate = DateTime.Now;
                        string dateFinish = errorDate.ToString("dd-MM-yyyy");
                        string dateFinishDateTime = errorDate.ToString("dd-MM-yyyy HH:mm:ss");
                        string logError  = @ConfigurationManager.AppSettings["log_error"];
                        StringBuilder errorr = new StringBuilder();
                        errorr.AppendLine("Falha no processamento");
                        errorr.AppendLine("Possível falha na criação do arquivo compactado na pasta destino");
                        errorr.AppendLine("Horário do Erro: "+dateFinishDateTime);
                        errorr.AppendLine("**************************************************************");
                        errorr.AppendLine("Especificação do erro:");
                        errorr.Append("Arquivo compactado já criado");
                        errorr.AppendLine("BATCH_CODE: "+ 0);
                        File.AppendAllText(logError+"error_"+dateFinish+".txt", errorr.ToString());
                        errorr.Clear();
                         
                     }
                 }else{
                    DateTime errorDate = DateTime.Now;
                    string dateFinish = errorDate.ToString("dd-MM-yyyy");
                    string dateFinishDateTime = errorDate.ToString("dd-MM-yyyy HH:mm:ss");
                    string logError  = @ConfigurationManager.AppSettings["log_error"];
                    StringBuilder erro = new StringBuilder();
                    erro.AppendLine("Falha no processamento");
                    erro.AppendLine("Possível falha no processamento da carga dos arquivos");
                    erro.AppendLine("Horário do Erro: "+dateFinishDateTime);
                    erro.AppendLine("**************************************************************");
                    erro.AppendLine("Especificação do erro:");
                    erro.Append("Não há arquivos para serem compactados na pasta raiz");
                    erro.AppendLine("BATCH_CODE: "+ 0);
                    File.AppendAllText(logError+"error_"+dateFinish+".txt", erro.ToString());
                    erro.Clear();
                 }

            }
            catch (Exception errorInfo)
            {
                DateTime errorDate = DateTime.Now;
                string dateFinish = errorDate.ToString("dd-MM-yyyy");
                string dateFinishDateTime = errorDate.ToString("dd-MM-yyyy HH:mm:ss");
                string logError  = @ConfigurationManager.AppSettings["log_error"];
                StringBuilder err = new StringBuilder();
                err.AppendLine("Falha no processamento");
                err.AppendLine("Possível falha no processamento da carga dos arquivos");
                err.AppendLine("Horário do Erro: "+dateFinishDateTime);
                err.AppendLine("**************************************************************");
                err.AppendLine("Especificação do erro:");
                err.Append(""+ errorInfo);
                File.AppendAllText(logError+"error_"+dateFinish+".txt",err.ToString());
                err.Clear();
                throw;
            }
        }
    }
}
